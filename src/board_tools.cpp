#include "board_tools.h"
#include "board_constants.h"

namespace B = board_constants;

namespace board_tools
{
    char getPieceIndexBySymbol(char symbol)
    {
        switch (tolower(symbol)) {
            case 'p':
                return B::PAWN;
            case 'n':
                return B::KNIGHT;
            case 'b':
                return B::BISHOP;
            case 'r':
                return B::ROOK;
            case 'q':
                return B::QUEEN;
            case 'k':
                return B::KING;
            default:
                throw std::exception();
        }
    }



    void visualizeBoard(Board board)
    {
        char canvas[65];
        canvas[64] = 0;
        for (int i = 0; i < 64; i++) {
            canvas[i] = '.';
        }

        for (int color = 0; color < 2; color++) {
            for (int piece_type = 0; piece_type < 6; piece_type++) {
                for (int square = 0; square < 64; square++) {
                    if (board.pieces[color][piece_type] & B::SQUARE[square]) {
                        char symbol = B::PIECE_SYMBOL[piece_type];
                        if (!color) {
                            symbol += 'a' - 'A';
                        }

                        canvas[square] = symbol;
                    }
                }
            }
        }

        for (int i = 56; i >= 0; i -= 8) {
            for (int j = 0; j < 8; j++) {
                std::cout << canvas[i + j] << " ";
            }
            std::cout << "\n";
        }

        std::cout << "\n";
    }
}