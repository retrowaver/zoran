#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include <cstdint>
#include "move.h"

#if !defined(BOARD_H)
#define BOARD_H 1

class Board
{
    public:
        Board();

        uint64_t pieces[2][6];
        bool turn;
        int en_passant_file; // 0 if none, 1-8 otherwise
        
        bool castling_rights[2][2]; // 0 for kingside, 1 for queenside

        std::stack <Move> moves;
        std::stack <MoveMeta> moves_meta;

        std::vector<Move> getPseudoLegalMoves();

        bool makeMove(Move move);
        void makePseudoLegalMove(Move move);
        void unmakeMove();
        bool wasMoveLegal();
//        bool isMoveCapture(Move move);


        char getPieceTypeAtSquare(int square, bool piece_color);


        uint64_t getPieces();
        uint64_t getPiecesByColor(bool piece_color);

        int getPieceCount(bool piece_color, char piece_type);

    private:
        void appendPseudoLegalMovesByPiece(std::vector<Move> &moves, char piece_type, uint64_t piece_bitboard, bool piece_color);
        void appendPseudoLegalMovesByPieceType(std::vector<Move> &moves, int piece_type);

        uint64_t getPawnForwardMovesBitboard(uint64_t piece_bitboard, bool piece_color);
        uint64_t getPawnCaptureMovesBitboard(uint64_t piece_bitboard, bool piece_color);
        uint64_t getPawnEnPassantMovesBitboard(uint64_t piece_bitboard, bool piece_color);
        uint64_t getBishopMovesBitboard(uint64_t piece_bitboard, bool piece_color);
        uint64_t getRookMovesBitboard(uint64_t piece_bitboard, bool piece_color);

        void appendPawnMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color);
        void appendKnightMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color);
        void appendBishopMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color);
        void appendRookMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color);
        void appendQueenMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color);
        void appendKingMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color);
        
        void appendMovesBitboardToMoves(std::vector<Move> &moves, uint64_t piece_bitboard, char piece_type, uint64_t moves_bitboard, bool is_en_passant, bool is_castling);

};

struct invalid_fen_exception : public std::exception {
    virtual const char* what() const noexcept {
        return "Invalid FEN";
    }
};



class FenBoardFactory
{
    public:
        static Board getBoardFromFen(std::string fen);
        static Board loadPieces(Board board, std::string fen_section);
        static Board loadPiece(Board board, char piece_symbol, char rank, char file);
        static Board loadTurn(Board board, std::string fen_section);
        static Board loadCastling(Board board, std::string fen_section);
        static Board loadEnPassant(Board board, std::string fen_section);
        static Board loadHalfmove(Board board, std::string fen_section);
        static Board loadFullmove(Board board, std::string fen_section);
};
#endif