#include "evaluator.h"
#include "board_constants.h"

namespace B = board_constants;

float Evaluator::evaluate(Board board) {
    float score = 0;

    score += (board.getPieceCount(B::WHITE, B::PAWN) - board.getPieceCount(B::BLACK, B::PAWN)) * 1;
    score += (board.getPieceCount(B::WHITE, B::KNIGHT) - board.getPieceCount(B::BLACK, B::KNIGHT)) * 3;
    score += (board.getPieceCount(B::WHITE, B::BISHOP) - board.getPieceCount(B::BLACK, B::BISHOP)) * 3;
    score += (board.getPieceCount(B::WHITE, B::ROOK) - board.getPieceCount(B::BLACK, B::ROOK)) * 5;
    score += (board.getPieceCount(B::WHITE, B::QUEEN) - board.getPieceCount(B::BLACK, B::QUEEN)) * 9;

    return score;
}