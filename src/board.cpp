#include "board.h"
#include <regex>
#include "board_constants.h"
#include "board_tools.h"
//#include <strings.h> //?
#include <string.h>

namespace B = board_constants;
namespace T = board_tools;

Board::Board()
{
    for (int color = 0; color < 2; color++) {
        for (int piece_type = 0; piece_type < 6; piece_type++) {
            this->pieces[color][piece_type] = 0;
        }
    }
}

bool Board::makeMove(Move move)
{
    this->makePseudoLegalMove(move);

    if (this->wasMoveLegal()) {
        return true;
    }

    this->unmakeMove();
    return false;
}

bool Board::wasMoveLegal()
{
    // This is probably super slow, but it's the simplest way to make it work as for now
    std::vector<Move> moves = this->getPseudoLegalMoves();
    for (auto & move: moves) {
        if (B::SQUARE[move.to] == this->pieces[!this->turn][B::KING]) {
            return false;
        }
    }

    return true;
}

void Board::makePseudoLegalMove(Move move)
{
    char captured_piece_type = move.is_en_passant ? B::PAWN : this->getPieceTypeAtSquare(move.to, !this->turn);

    MoveMeta move_meta;
    move_meta.captured_piece_type = captured_piece_type;
    move_meta.previous_en_passant_file = this->en_passant_file;

    //
    move_meta.revoked_kingside_castling = false;
    move_meta.revoked_queenside_castling = false;
    if (move.piece_type == B::KING) {
        // If the king is moved, then mark castle rights as revoked - but only if they existed beforehand
        move_meta.revoked_kingside_castling = this->castling_rights[this->turn][0];
        move_meta.revoked_queenside_castling = this->castling_rights[this->turn][1];
    } else if (move.piece_type == B::ROOK) {
        if (this->castling_rights[this->turn][0]) {
            int kingside_rook_square = this->turn == B::WHITE ? B::H1 : B::H8;
            move_meta.revoked_kingside_castling = this->castling_rights[this->turn][0] && move.from == kingside_rook_square;
        }

        if (this->castling_rights[this->turn][1]) {
            int queenside_rook_square = this->turn == B::WHITE ? B::A1 : B::A8;
            move_meta.revoked_queenside_castling = this->castling_rights[this->turn][1] && move.from == queenside_rook_square;
        }
    }

    // Push moves info to stacks
    this->moves.push(move);
    this->moves_meta.push(move_meta);

    // Remove piece from initial square
    this->pieces[this->turn][move.piece_type] &= ~B::SQUARE[move.from];

    // Remove captured piece
    if (captured_piece_type != 6) { // 6 => non capture move
        if (move.is_en_passant) {
            int en_passant_capture_square = move.from + (move.from % 8 > move.to % 8 ? -1 : 1);
            this->pieces[!this->turn][captured_piece_type] &= ~B::SQUARE[en_passant_capture_square];
        } else {
            this->pieces[!this->turn][captured_piece_type] &= ~B::SQUARE[move.to];
        }
    }

    // Place piece at new square
    this->pieces[this->turn][move.piece_type] |= B::SQUARE[move.to];

    // Update castling
    if (move_meta.revoked_kingside_castling) {
        this->castling_rights[this->turn][0] = false;
    }

    if (move_meta.revoked_queenside_castling) {
        this->castling_rights[this->turn][1] = false;
    }

    // Update rook after castling
    if (move.is_castling) {
        if (move.to == B::G1 || move.to == B::G8) {
            // kingside
            this->pieces[this->turn][B::ROOK] |= B::SQUARE[move.to - 1];
            this->pieces[this->turn][B::ROOK] &= ~B::SQUARE[move.to + 1];
        } else {
            // queenside
            this->pieces[this->turn][B::ROOK] |= B::SQUARE[move.to + 1];
            this->pieces[this->turn][B::ROOK] &= ~B::SQUARE[move.to - 2];
        }
    }

    // Update en passant file
    if (move.piece_type == B::PAWN && (abs(move.from - move.to) == 16)) {
        this->en_passant_file = move.to % 8 + 1; // because 0 is reserved for no en passant
    } else {
        this->en_passant_file = 0;
    }

    // Update turn
    this->turn = !this->turn;
}

char Board::getPieceTypeAtSquare(int square, bool piece_color)
{
    char piece_type;

    for (piece_type = 0; piece_type < 6; piece_type++) {
        if ((B::SQUARE[square] & this->pieces[piece_color][piece_type]) > 0) {
            return piece_type;
        }
    }

    return 6; // 6 for no piece at that square
}

void Board::unmakeMove()
{   
    Move move = this->moves.top();
    MoveMeta move_meta = this->moves_meta.top();


    // Update turn
    this->turn = !this->turn;

    // Update en passant file
    this->en_passant_file = move_meta.previous_en_passant_file;

    // Update castling rights
    if (move_meta.revoked_kingside_castling) {
        this->castling_rights[this->turn][0] = true;
    }

    if (move_meta.revoked_queenside_castling) {
        this->castling_rights[this->turn][1] = true;
    }

    // Update pieces

    // Move piece back
    this->pieces[this->turn][move.piece_type] &= ~B::SQUARE[move.to];
    this->pieces[this->turn][move.piece_type] |= B::SQUARE[move.from];

    // Place captured piece
    if (move_meta.captured_piece_type != 6) { // 6 => non capture move
        if (move.is_en_passant) {
            int en_passant_capture_square = move.from + (move.from % 8 > move.to % 8 ? -1 : 1);
            this->pieces[!this->turn][move_meta.captured_piece_type] |= B::SQUARE[en_passant_capture_square];
        } else {
            this->pieces[!this->turn][move_meta.captured_piece_type] |= B::SQUARE[move.to];
        }
    }

    // Update rook after castling
    if (move.is_castling) {
        if (move.to == B::G1 || move.to == B::G8) {
            // kingside
            this->pieces[this->turn][B::ROOK] &= ~B::SQUARE[move.to - 1];
            this->pieces[this->turn][B::ROOK] |= B::SQUARE[move.to + 1];
        } else {
            // queenside
            this->pieces[this->turn][B::ROOK] &= ~B::SQUARE[move.to + 1];
            this->pieces[this->turn][B::ROOK] |= B::SQUARE[move.to - 2];
        }
    }
    

    this->moves.pop();
    this->moves_meta.pop();
}

std::vector<Move> Board::getPseudoLegalMoves()
{
    std::vector<Move> moves;

    for (int piece_type = 0; piece_type < 6; piece_type++) {
        this->appendPseudoLegalMovesByPieceType(moves, piece_type);
    }

    return moves;
}

void Board::appendPseudoLegalMovesByPieceType(std::vector<Move> &moves, int piece_type)
{
    uint64_t piece_bitboard = this->pieces[this->turn][piece_type];
    while (piece_bitboard) {
        uint64_t single_piece_bitboard = piece_bitboard & -piece_bitboard;
        this->appendPseudoLegalMovesByPiece(
            moves,
            piece_type,
            single_piece_bitboard,
            this->turn
        );

        piece_bitboard &= piece_bitboard - 1;
    }
}

void Board::appendMovesBitboardToMoves(std::vector<Move> &moves, uint64_t piece_bitboard, char piece_type, uint64_t moves_bitboard, bool is_en_passant, bool is_castling)
{
    int index;
    int move_from = ffsll(piece_bitboard) - 1;

    while (index = ffsll(moves_bitboard)) {
        Move move;
        move.from = move_from;
        move.to = index - 1;
        move.is_en_passant = is_en_passant;
        move.is_castling = is_castling;
        move.piece_type = piece_type;

        moves.push_back(move);

        moves_bitboard &= ~B::SQUARE[index - 1];
    }
}

void Board::appendPseudoLegalMovesByPiece(
    std::vector<Move> &moves,
    char piece_type,
    uint64_t piece_bitboard,
    bool piece_color
) {
    switch (piece_type) {
        case B::PAWN:
            this->appendPawnMoves(moves, piece_bitboard, piece_color);
            break;

        case B::KNIGHT:
            this->appendKnightMoves(moves, piece_bitboard, piece_color);
            break;

        case B::BISHOP:
            this->appendBishopMoves(moves, piece_bitboard, piece_color);
            break;

        case B::ROOK:
            this->appendRookMoves(moves, piece_bitboard, piece_color);
            break;

        case B::QUEEN:
            this->appendQueenMoves(moves, piece_bitboard, piece_color);
            break;

        case B::KING:
            this->appendKingMoves(moves, piece_bitboard, piece_color);
            break;
    }
}

uint64_t Board::getPawnForwardMovesBitboard(uint64_t piece_bitboard, bool piece_color)
{
    uint64_t moves_bitboard = 0;
    
    int square_index = ffsll(piece_bitboard) - 1;

    // Forward moves
    int forward_square_index = square_index + (piece_color ? 8 : -8);
    uint64_t all_pieces_bitboard = this->getPieces();

    if ((B::SQUARE[forward_square_index] & all_pieces_bitboard) == 0) {
        moves_bitboard |= B::SQUARE[forward_square_index];

        // 2 squares forward (from 2nd rank for White and from 7th for Black)
        if (
            ((B::SQUARE[square_index] & B::RANK_2) && piece_color == B::WHITE)
            || ((B::SQUARE[square_index] & B::RANK_7) && piece_color == B::BLACK)
        ) {
            int twice_forward_square_index = forward_square_index + (piece_color ? 8 : -8);
            if ((B::SQUARE[twice_forward_square_index] & all_pieces_bitboard) == 0) {
                moves_bitboard |= B::SQUARE[twice_forward_square_index];
            }
        }
    }

    return moves_bitboard;
}

uint64_t Board::getPawnCaptureMovesBitboard(uint64_t piece_bitboard, bool piece_color)
{
    int square_index = ffsll(piece_bitboard) - 1;

    uint64_t capture_bitboard = piece_color ? B::PAWN_WHITE_CAPTURE_MOVES[square_index] : B::PAWN_BLACK_CAPTURE_MOVES[square_index];

    return capture_bitboard & this->getPiecesByColor(!piece_color);
}

uint64_t Board::getPawnEnPassantMovesBitboard(uint64_t piece_bitboard, bool piece_color)
{
    if (!this->en_passant_file) {
        return 0;
    }

    int square_index = ffsll(piece_bitboard) - 1;
    uint64_t capture_bitboard = piece_color ? B::PAWN_WHITE_CAPTURE_MOVES[square_index] : B::PAWN_BLACK_CAPTURE_MOVES[square_index];
    uint64_t rank_bitboard = this->turn ? B::RANK_6 : B::RANK_3; // rank of target square

    return (capture_bitboard & rank_bitboard & B::FILE[this->en_passant_file - 1]);
}

void Board::appendPawnMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color)
{
    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::PAWN,
        this->getPawnForwardMovesBitboard(piece_bitboard, piece_color) | this->getPawnCaptureMovesBitboard(piece_bitboard, piece_color),
        0,
        0
    );

    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::PAWN,
        this->getPawnEnPassantMovesBitboard(piece_bitboard, piece_color),
        1,
        0
    );
}

void Board::appendKnightMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color)
{
    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::KNIGHT,
        B::KNIGHT_MOVES[ffsll(piece_bitboard) - 1] & ~this->getPiecesByColor(piece_color),
        0,
        0
    );
}

uint64_t Board::getBishopMovesBitboard(uint64_t piece_bitboard, bool piece_color)
{
    uint64_t moves_bitboard = 0;

    int square_index = ffsll(piece_bitboard) - 1;
    int current_index;

    // NE
    current_index = square_index;
    bool end = false;
    while (!end) {
        current_index = current_index + 8 + 1;

        if (current_index % 8 == 0 || current_index > 63) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    // SE
    current_index = square_index;
    end = false;
    while (!end) {
        current_index = current_index - 8 + 1;

        if (current_index % 8 == 0 || current_index < 0) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    // SW
    current_index = square_index;
    end = false;
    while (!end) {
        current_index = current_index - 8 - 1;

        if ((current_index + 1) % 8 == 0 || current_index < 0) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    // NW
    current_index = square_index;
    end = false;
    while (!end) {
        current_index = current_index + 8 - 1;

        if ((current_index + 1) % 8 == 0 || current_index > 63) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    return moves_bitboard;
}

void Board::appendBishopMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color)
{
    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::BISHOP,
        this->getBishopMovesBitboard(piece_bitboard, piece_color),
        0,
        0
    );
}

uint64_t Board::getRookMovesBitboard(uint64_t piece_bitboard, bool piece_color)
{
    uint64_t moves_bitboard = 0;

    int square_index = ffsll(piece_bitboard) - 1;
    int current_index;

    // East
    current_index = square_index;
    bool end = false;
    while (!end) {
        current_index++;

        if (current_index % 8 == 0) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    // West
    current_index = square_index;
    end = false;
    while (!end) {
        current_index--;

        if ((current_index + 1) % 8 == 0) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    // North
    current_index = square_index;
    end = false;
    while (!end) {
        current_index += 8;

        if (current_index > 63) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    // South
    current_index = square_index;
    end = false;
    while (!end) {
        current_index -= 8;

        if (current_index < 0) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(piece_color)) {
            break;
        }

        if (B::SQUARE[current_index] & this->getPiecesByColor(!piece_color)) {
            end = true;
        }

        moves_bitboard |= B::SQUARE[current_index];
    }

    return moves_bitboard;
}

void Board::appendRookMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color)
{
    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::ROOK,
        this->getRookMovesBitboard(piece_bitboard, piece_color),
        0,
        0
    );
}

void Board::appendQueenMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color)
{
    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::QUEEN,
        this->getBishopMovesBitboard(piece_bitboard, piece_color) | this->getRookMovesBitboard(piece_bitboard, piece_color),
        0,
        0
    );
}

void Board::appendKingMoves(std::vector<Move> &moves, uint64_t piece_bitboard, bool piece_color)
{
    this->appendMovesBitboardToMoves(
        moves,
        piece_bitboard,
        B::KING,
        B::KING_MOVES[ffsll(piece_bitboard) - 1] & ~this->getPiecesByColor(piece_color),
        0,
        0
    );

    // castling

    
    if (this->castling_rights[this->turn][0]) {
        if (!(B::EMPTY_CASTLING_SQUARES[this->turn][0] & (this->getPiecesByColor(B::WHITE) | this->getPiecesByColor(B::BLACK)))) {
            this->appendMovesBitboardToMoves(
                moves,
                piece_bitboard,
                B::KING,
                B::CASTLING_TO_SQUARES[this->turn][0],
                0,
                1
            );
        }
    }
    
    if (this->castling_rights[this->turn][1]) {
        if (!(B::EMPTY_CASTLING_SQUARES[this->turn][1] & (this->getPiecesByColor(B::WHITE) | this->getPiecesByColor(B::BLACK)))) {
            this->appendMovesBitboardToMoves(
                moves,
                piece_bitboard,
                B::KING,
                B::CASTLING_TO_SQUARES[this->turn][1],
                0,
                1
            );
        }
    }
}

uint64_t Board::getPiecesByColor(bool piece_color)
{
    return (
        this->pieces[piece_color][B::PAWN]
        | this->pieces[piece_color][B::KNIGHT]
        | this->pieces[piece_color][B::BISHOP]
        | this->pieces[piece_color][B::ROOK]
        | this->pieces[piece_color][B::QUEEN]
        | this->pieces[piece_color][B::KING]
    );
}

uint64_t Board::getPieces()
{
    return (this->getPiecesByColor(B::WHITE) | this->getPiecesByColor(B::BLACK));
}

int Board::getPieceCount(bool piece_color, char piece_type)
{
    int count = 0;
    
    uint64_t piece_bitboard = this->pieces[piece_color][piece_type];
    while (piece_bitboard) {
       count++;
       piece_bitboard &= piece_bitboard - 1; // reset LS1B
   }
   return count;
}





Board FenBoardFactory::getBoardFromFen(std::string fen)
{
    std::regex test("((?:[pPnNbBrRqQkK1-8]{1,8}\\/){7}[pPnNbBrRqQkK1-8]{1,8}) ([bw]{1}) ((?=.)K?Q?k?q?|-) ([a-h][36]|-) ([1-9][0-9]*|[0]) ([1-9][0-9]*)");
    std::smatch result;
    bool success = std::regex_match(fen, result, test);

    if (!success) {
        throw invalid_fen_exception();
    }

    Board board;
    board = FenBoardFactory::loadPieces(board, result[1]);
    board = FenBoardFactory::loadTurn(board, result[2]);
    board = FenBoardFactory::loadCastling(board, result[3]);
    board = FenBoardFactory::loadEnPassant(board, result[4]);
    board = FenBoardFactory::loadHalfmove(board, result[5]);
    board = FenBoardFactory::loadFullmove(board, result[6]);

    return board;
}

Board FenBoardFactory::loadPieces(Board board, std::string fen_section)
{
    char rank = 8;
    char file = 1;
    for (auto c : fen_section) {
        if (file > 9 || (file == 9 && c != '/')) {
            throw invalid_fen_exception();
        }

        if (c == '/') {
            rank--;
            file = 1;
            continue;
        }

        if (isdigit(c)) {
            file += c - '0';
            continue;
        }

        board = FenBoardFactory::loadPiece(board, c, rank, file);
        file++;
    }

    return board;
}

Board FenBoardFactory::loadPiece(Board board, char piece_symbol, char rank, char file)
{
    bool color_index = isupper(piece_symbol);
    char piece_index = T::getPieceIndexBySymbol(piece_symbol);
    char square_index = (rank - 1) * 8 + file - 1;

    board.pieces[color_index][piece_index] |= B::SQUARE[square_index];
    
    return board;
}

Board FenBoardFactory::loadTurn(Board board, std::string fen_section)
{
    if (fen_section[0] == 'w') {
        board.turn = 1;
    } else {
        board.turn = 0;
    }

    return board;
}

Board FenBoardFactory::loadCastling(Board board, std::string fen_section)
{
    board.castling_rights[B::WHITE][0] = (strchr(fen_section.c_str(), 'K') != nullptr);
    board.castling_rights[B::WHITE][1] = (strchr(fen_section.c_str(), 'Q') != nullptr);
    board.castling_rights[B::BLACK][0] = (strchr(fen_section.c_str(), 'k') != nullptr);
    board.castling_rights[B::BLACK][1] = (strchr(fen_section.c_str(), 'q') != nullptr);

    return board;
}

Board FenBoardFactory::loadEnPassant(Board board, std::string fen_section)
{
    if (fen_section[0] == '-') {
        board.en_passant_file = 0;
        return board;
    }

    char file_index = fen_section[0] - 'a' + 1;

    if (file_index < 1 || file_index > 8) {
        throw invalid_fen_exception();
    }

    board.en_passant_file = file_index;

    return board;
}

Board FenBoardFactory::loadHalfmove(Board board, std::string fen_section)
{
    return board;
}

Board FenBoardFactory::loadFullmove(Board board, std::string fen_section)
{
    return board;
}