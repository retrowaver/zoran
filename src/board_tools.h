#include "board_constants.h"
#include "board.h"

#if !defined(BOARD_TOOLS_H)
#define BOARD_TOOLS_H 1

namespace board_tools
{
    char getPieceIndexBySymbol(char symbol);
    void visualizeBoard(Board board);
}
#endif