#include <algorithm>

#include "search.h"
#include "board_constants.h"
#include "move.h"

namespace B = board_constants;

Search::Search(Evaluator evaluator) {
    this->evaluator = evaluator;
}

float Search::negamax(Board board, int depthLeft, float alpha, float beta) {
    // TODO ending condition based on move repetition / 50 move rule / stuff like that

    /* Ending condition check - START */
    bool no_legal_moves = true;
    std::vector<Move> moves = board.getPseudoLegalMoves();
    for (auto & move: moves) {
        if (board.makeMove(move)) {
            board.unmakeMove();
            no_legal_moves = false;
            break;
        } else {
            // illegal moves removal here
        }
    }

    if (no_legal_moves) {
        // stalemate or checkmate

        board.turn = !board.turn;
        std::vector<Move> opponent_moves = board.getPseudoLegalMoves();
        board.turn = !board.turn;

        for (auto & move: opponent_moves) {
            if (B::SQUARE[move.to] == board.pieces[board.turn][B::KING]) {
                return -9999; // checkmate
            }
        }

        return 0; // stalemate
    }
    /* Ending condition check - END */


    if (depthLeft == 0) {
        float score = this->evaluator.evaluate(board);
        return score * (board.turn == B::WHITE ? 1 : -1);
    }

    float value = -99999;

    for (auto & move: moves) {
        bool success = board.makeMove(move);
        if (!success) {
            continue;
        }

        value = std::max(value, -this->negamax(board, depthLeft - 1, -beta, -alpha));
        alpha = std::max(alpha, value);

        board.unmakeMove();

        if (alpha >= beta) {
            break;
        }
    }

    return value;
}