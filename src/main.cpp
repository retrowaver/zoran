#include <iostream>
#include "board.h"
#include "board_constants.h"
#include "board_tools.h"
#include "move.h"
#include "evaluator.h"
#include "search.h"

#include <chrono>

using namespace std::chrono;

int main()
{
    
    //Board board = FenBoardFactory::getBoardFromFen("8/p7/8/8/8/8/PP6/8 w KQkq - 0 1");
    //Board board = FenBoardFactory::getBoardFromFen("1r5k/6pp/2pr4/P1Q3bq/1P2B3/2P5/4nPPP/R3NR1K b - - 0 1");
    Board board = FenBoardFactory::getBoardFromFen("7k/5Kp1/5p1p/5P1P/8/8/8/8 w - - 0 1");

    

    Evaluator evaluator;
    Search search(evaluator);
    std::cout << (search.negamax(board, 6, -99999, 99999) * (board.turn ? 1 : -1)) << "\n";
    return 0;
    

    //board_tools::visualizeBoard(board);
    
    //Board board = FenBoardFactory::getBoardFromFen("8/p7/8/8/8/8/PP6/8 w KQkq - 0 1");

    /*Board board = FenBoardFactory::getBoardFromFen("rnbqkbnr/1ppppppp/8/p7/8/PP6/2PPPPPP/RNBQKBNR b KQkq - 0 1");
    board_tools::visualizeBoard(board);

    Move move;
    move.from = board_constants::A5;
    move.to = board_constants::A4;
    move.piece_type = board_constants::PAWN;
    move.is_en_passant = false;
    move.is_castling = false;

    board.makeMove(move);*/

    //board.makePseudoLegalMove(move);
    //board.makeMove(move);

    /*
            int to;
        int from;
        char piece_type;
        bool is_en_passant;
        bool is_castling;

        */


    //std::cout << board.en_passant_file << "\n";
    //return 0;


    board_tools::visualizeBoard(board);

    //board.unmakeMove();

    //board_tools::visualizeBoard(board);


    /*Move move;
    move.from = board_constants::G4;
    move.to = board_constants::H3;
    move.is_en_passant = true;

    board.makePseudoLegalMove(move);
    board_tools::visualizeBoard(board);

    std::cout << "\n____\n" << +board.moves_meta.top().previous_en_passant_file << "\n_____\n";*/


/*

    Move move2;
    move2.to = board_constants::D5;
    move2.from = board_constants::D7;
    move2.is_en_passant = false;

    board.makePseudoLegalMove(move2);
    board_tools::visualizeBoard(board);




    Move move3;
    move3.to = board_constants::D5;
    move3.from = board_constants::C4;
    move3.is_en_passant = false;

    board.makePseudoLegalMove(move3);
    board_tools::visualizeBoard(board);*/





    /*milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    uint64_t start = ms.count();

    for (int i = 0; i < 1000000; i++) {
        board.getPseudoLegalMoves();
        //board.makeMove(move);
        //board.unmakeMove();
    }

    ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    std::cout << ((float)(ms.count() - start)/1000) << "\n";*/


    //std::cout << moves.size();





    std::vector<Move> moves = board.getPseudoLegalMoves();
    for (auto & value: moves) {
        std::cout << value.to_textual() << "\n";
    }

}