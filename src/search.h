#include "board.h"
#include "evaluator.h"

#if !defined(SEARCH_H)
#define SEARCH_H 1

class Search
{
    public:
        Search(Evaluator evaluator);
        float negamax(Board board, int depthLeft, float alpha, float beta);

    private:
        Evaluator evaluator;
};

#endif