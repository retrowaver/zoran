#include "board.h"

#if !defined(EVALUATOR_H)
#define EVALUATOR_H 1

class Evaluator
{
    public:
        float evaluate(Board board);
};

#endif