#include <string>

#if !defined(MOVE_H)
#define MOVE_H 1

class Move
{
    public:
        int to;
        int from;
        char piece_type;
        bool is_en_passant;
        bool is_castling;

        std::string to_textual();
};

class MoveMeta
{
    public:
        char captured_piece_type;
        bool revoked_queenside_castling;
        bool revoked_kingside_castling;
        char previous_en_passant_file;
};

#endif