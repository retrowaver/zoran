#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/board_constants.h"
#include "../src/board.h"

namespace B = board_constants;

TEST_CASE("throws invalid fen exception on entirely invalid fen") {
    REQUIRE_THROWS_AS(FenBoardFactory::getBoardFromFen(""), invalid_fen_exception);
    REQUIRE_THROWS_AS(FenBoardFactory::getBoardFromFen("123"), invalid_fen_exception);
    REQUIRE_THROWS_AS(FenBoardFactory::getBoardFromFen("definitely not valid"), invalid_fen_exception);
}

TEST_CASE("loadTurn loads turn correctly") {
    Board board;

    board = FenBoardFactory::loadTurn(board, "w");
    REQUIRE(board.turn == 1);

    board = FenBoardFactory::loadTurn(board, "b");
    REQUIRE(board.turn == 0);
}

TEST_CASE("loadPieces loads pieces correctly") {
    Board board1, board2;

    board1 = FenBoardFactory::loadPieces(board1, "k7/2r5/8/8/8/8/4R3/K7");
    REQUIRE(board1.pieces[B::WHITE][B::ROOK] == B::SQUARE[B::E2]);
    REQUIRE(board1.pieces[B::BLACK][B::ROOK] == B::SQUARE[B::C7]);
    REQUIRE(board1.pieces[B::WHITE][B::KING] == B::SQUARE[B::A1]);
    REQUIRE(board1.pieces[B::BLACK][B::KING] == B::SQUARE[B::A8]);

    board2 = FenBoardFactory::loadPieces(board2, "6k1/5ppp/8/8/8/8/5PPP/6K1");
    REQUIRE(board2.pieces[B::WHITE][B::KING] == B::SQUARE[B::G1]);
    REQUIRE(board2.pieces[B::BLACK][B::KING] == B::SQUARE[B::G8]);
    REQUIRE(board2.pieces[B::WHITE][B::PAWN] == (B::SQUARE[B::F2] | B::SQUARE[B::G2] | B::SQUARE[B::H2]));
    REQUIRE(board2.pieces[B::BLACK][B::PAWN] == (B::SQUARE[B::F7] | B::SQUARE[B::G7] | B::SQUARE[B::H7]));
}

TEST_CASE("loadPieces throws exception on invalid data (too wide rows)") {
    Board board;
    REQUIRE_THROWS_AS(FenBoardFactory::loadPieces(board, "8/8/8/8/7k7/8/8/8"), invalid_fen_exception);
}